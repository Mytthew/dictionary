package com.company;

public class PolishDictionary extends AbstractDictionary {
	private static int userCounter = 0;

	public PolishDictionary() {
		userCounter++;
	}

	public static int getUserCounter() {
		return userCounter;
	}

	@Override
	public String translate(String word) {
		return getDict().get(word);
	}
}

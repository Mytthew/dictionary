package com.company;

import java.util.HashMap;
import java.util.Map;

abstract class AbstractDictionary {
	private static int userCounter;
	private final int serialNumber = userCounter;
	private final Map<String, String> dict = new HashMap<>();

	{
		dict.put("Północ", "North");
		dict.put("Południe", "South");
		dict.put("Wschód", "East");
		dict.put("Zachód", "West");
	}

	public AbstractDictionary() {
		userCounter++;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public static int getUserCounter() {
		return userCounter;
	}

	public Map<String, String> getDict() {
		return dict;
	}

	public abstract String translate(String word);
}

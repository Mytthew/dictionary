package com.company;

public class Main {
    public static void main(String[] args) {
		PolishDictionary p1 = new PolishDictionary();
		int beginningSerialNumber = p1.getSerialNumber();
		PolishDictionary p2 = new PolishDictionary();
		if (beginningSerialNumber != p1.getSerialNumber()) {
			System.out.println("1");
		}

		if (p1.getSerialNumber() == p2.getSerialNumber()) {
			System.out.println("2");
		}
		if (PolishDictionary.getUserCounter() != 2) {
			System.out.println("3");
		}
		if (AbstractDictionary.getUserCounter() != 2) {
			System.out.println("4");
		}

		AbstractDictionary d1 = new AbstractDictionary() {
			@Override
			public String translate(String word) {
				return null;
			}
		};

		if (beginningSerialNumber != p1.getSerialNumber()) {
			System.out.println("5");
		}
		if (PolishDictionary.getUserCounter() != 2) {
			System.out.println("6");
		}
		if (AbstractDictionary.getUserCounter() != 3) {
			System.out.println("7");
		}
		if (d1.getSerialNumber() == p1.getSerialNumber()) {
			System.out.println("8");
		}
		if (d1.getSerialNumber() == p2.getSerialNumber()) {
			System.out.println("9");
		}
		System.out.println("OK");
    }
}
